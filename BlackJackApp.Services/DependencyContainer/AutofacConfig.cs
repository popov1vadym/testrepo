﻿using Autofac;
using BlackJackApp.DAL.DepedencyContainer;
using BlackJackApp.Services;
using BlackJackApp.Services.Interfaces;
using BlackJackApp.Services.Services;

namespace BlackJackApp.Controllers.Util
{
    public static class AutofacConfig
    {
        public static void ConfigureContainer(ContainerBuilder builder, string connectionString)
        {
            AutofacConfiguration.ConfigureContainer(builder, connectionString);
            builder.RegisterType<GameService>().As<IGameService>();
            builder.RegisterType<HistoryService>().As<IHistoryService>();
        }
    }
}