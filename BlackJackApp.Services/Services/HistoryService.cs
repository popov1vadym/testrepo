﻿using BlackJackApp.DataAccess.Repositories.Interfaces;
using BlackJackApp.Entities.Entities;
using BlackJackApp.Services.Interfaces;
using BlackJackApp.ViewModels.HistoryModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJackApp.Services.Services
{
    public class HistoryService : IHistoryService
    {
        private IGameRepository _gameRepository;
        private IRoundRepository _roundRepository;
        private IPlayerInGameRepository _playersGameRepository;

        public HistoryService(IGameRepository gameRepository, 
                            IRoundRepository roundRepository, 
                            IPlayerInGameRepository playerGameRepository)
        {
            _gameRepository = gameRepository;
            _roundRepository = roundRepository;
            _playersGameRepository = playerGameRepository;
        }

        public async Task<GetGamesHistoryView> GetGames(int offset)
        {
            var query = await _gameRepository.GetLastTen(offset);

            return await CreateGameHistoryViewModel(query);
        }

        private async Task<GetGamesHistoryView> CreateGameHistoryViewModel(IEnumerable<Game> games)
        {
            var gameView = new GetGamesHistoryView();
            var listOfViewModel = new List<GetGamesHistoryViewItem>();
            foreach (var game in games)
            {
                PlayerInGame playerGames = await GetDetailsFromGame(game.Id);

                var gameViewModel = new GetGamesHistoryViewItem();
                gameViewModel.Name = playerGames.Player.Name;
                gameViewModel.GameCreationDate = playerGames.Game.CreationDate;
                gameViewModel.GameId = game.Id;
                listOfViewModel.Add(gameViewModel);
            }
            gameView.Games = listOfViewModel;

            return gameView;
        }

        private async Task<PlayerInGame> GetDetailsFromGame(int gameId)
        {
            return await _playersGameRepository.Get(gameId);
        }

        private async Task<IEnumerable<Round>> GetAllRoundsFromParticularGame(int gameId)
        {
            var query = await _roundRepository.GetRounds(gameId);
            
            return query;
        }

        public async Task<DetailsHistoryView> Details(int gameId)
        {

            var rounds = await GetAllRoundsFromParticularGame(gameId);

            var result = rounds.GroupBy(p => p.Player.Name);
            var playersGame = new PlayerInGame();

            var viewDetails = new DetailsHistoryView();
            var userModelList = new List<DetailHistoryViewItem>();

            foreach (var round in result)
            {
                var userModel = new DetailHistoryViewItem();
                userModel.UserName = round.Key;

                foreach (var item in round)
                {
                    var cardViewModel = new CardHistoryViewItem();

                    cardViewModel.Rank = item.Card.Rank.ToString();
                    cardViewModel.Suit = item.Card.Suit.ToString();
                    userModel.CardSum += item.Card.Value;
                    playersGame = await GetStatusInParticularGame(item.PlayerId, item.GameId);
                    userModel.Cards.Add(cardViewModel);
                }
                userModel.Status = playersGame.Status.ToString();
                userModelList.Add(userModel);
            }
            viewDetails.HistoryDetails = userModelList;
            return viewDetails;
        }

        private async Task<PlayerInGame> GetStatusInParticularGame(int playerId, int gameId)
        {
            return await _playersGameRepository.GetStatus(playerId, gameId);
        }
    }
}
