﻿namespace BlackJackApp.Configuration
{
    public static class Config
    {
        public const int twentyOnePoint = 21;
        public const int dealerPointBorder = 17;
    }
}
