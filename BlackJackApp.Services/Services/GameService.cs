﻿using BlackJackApp.Entities.Entities;
using BlackJackApp.Entities.Enums;
using BlackJackApp.Services.Enums;
using BlackJackApp.ViewModels.GameModels;
using BlackJackApp.Services.Interfaces;
using BlackJackApp.ViewModels.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackJackApp.DataAccess.Repositories.Interfaces;

namespace BlackJackApp.Services
{
    public class GameService : IGameService
    {
        private IGameRepository _gameRepository;
        private IRoundRepository _roundRepository;
        private IPlayerRepository _playerRepository;
        private ICardRepository _cardRepository;
        private IPlayerInGameRepository _playersGameRepository;
        private const int DealerPointBorder = 17;
        private const int TwentyOnePoint = 21;
        private const int DealerAndPlayer = 2;

        public GameService(IGameRepository gameRepository,
                            IPlayerRepository playerRepository,
                            IRoundRepository roundRepository,
                            ICardRepository cardRepository,
                            IPlayerInGameRepository playersGameRepository)
        {
            _gameRepository = gameRepository;
            _playerRepository = playerRepository;
            _roundRepository = roundRepository;
            _cardRepository = cardRepository;
            _playersGameRepository = playersGameRepository;
        }

        public async Task<int> Create(CreateGameView viewFromUI)
        {
            int gameId = await CreateGame(viewFromUI.PlayerName, viewFromUI.BotQuantity);
            var rounds = new List<Round>();

            rounds.AddRange(await CreateHuman(viewFromUI.PlayerName, gameId));
            if (viewFromUI.BotQuantity != 0)
            {
                rounds.AddRange(await CreateBots(viewFromUI.BotQuantity, gameId));
            }
            rounds.AddRange(await CreateDealer(gameId));

            return gameId;
        }

        private async Task<PlayGameView> GetFirstRound(int id)
        {
            var rounds = await _roundRepository.GetRounds(id);

            var roundModel = await MappingToViewModel(rounds);

            await CheckRules(roundModel);

            return await CompleteRound(roundModel);
        }

        public async Task<PlayGameView> Play(int id, GetCardOption param)
        {
            if (param == GetCardOption.Start)
            {
                return await GetFirstRound(id);
            }
            if (param == GetCardOption.Take)
            {
                return await CreateNextRoundForPlayers(id);
            }
            if (param == GetCardOption.Enough)
            {
                return await CreateNextRoundForDealer(id);
            }
            return new PlayGameView();
        }

        public async Task<GetPlayersGameView> GetPlayers()
        {
            var result = await _playerRepository.GetAll();

            var users = new GetPlayersGameView();
            foreach (var name in result)
            {
                var usersViewItem = new GetPlayersGameViewItem();
                usersViewItem.Name = name;
                users.Users.Add(usersViewItem);
            }
            return users;
        }

        private async Task<PlayGameView> CompleteRound(PlayGameView roundModel)
        {
            var humanPlayer = GetHumanPlayer(roundModel.Users);
            var dealer = GetDealer(roundModel.Users);

            if (humanPlayer.PlayerStatus == PlayerStatusEnumView.Lose &&
                dealer.PlayerStatus == PlayerStatusEnumView.DefaultValue)
            {
                if (roundModel.Users.Count > DealerAndPlayer)
                {
                    await CheckDealer(roundModel.Users);
                    await FinalPointsCount(roundModel);
                }
                roundModel.IsResultComplete = true;
                return roundModel;
            }
            if (dealer.PlayerStatus == PlayerStatusEnumView.Winner)
            {
                var players = roundModel.Users.Where(r => r.PlayerStatus == PlayerStatusEnumView.DefaultValue);

                foreach (var player in players)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Lose;
                };
                roundModel.IsResultComplete = true;

                return roundModel;
            }

            if (dealer.PlayerStatus == PlayerStatusEnumView.Lose)
            {
                var players = roundModel.Users.Where(r => r.PlayerStatus == PlayerStatusEnumView.DefaultValue);
                foreach (var player in roundModel.Users)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Winner;
                };
                roundModel.IsResultComplete = true;
                return roundModel;
            }

            if (humanPlayer.PlayerStatus == PlayerStatusEnumView.Winner)
            {
                return await FinalPointsCount(roundModel);
            }
            UpdateStatus(roundModel);
            return roundModel;
        }

        private async Task<PlayGameView> CreateNextRoundForPlayers(int gameId)
        {
            var rounds = new List<Round>();

            rounds = await _roundRepository.GetRounds(gameId);

            var result = rounds.GroupBy(r => r.Player.Name);

            foreach (var item in result)
            {
                int cardSum = item.Sum(x => x.Card.Value);
                var player = item.First().Player;

                if (player.PlayerRole != PlayerRole.Dealer &&
                    cardSum < DealerPointBorder)
                {
                    rounds.Add(await CreateNextRoundForPlayers(player, gameId));
                }
            }

            var roundModel = await MappingToViewModel(rounds);

            await CheckRules(roundModel);
            UpdateStatus(roundModel);
            return await CompleteRound(roundModel);
        }

        private async Task<PlayGameView> CreateNextRoundForDealer(int gameId)
        {
            var rounds = new List<Round>();
            rounds = await _roundRepository.GetRounds(gameId);
            var roundModel = await MappingToViewModel(rounds);

            await CheckDealer(roundModel.Users);
            return await FinalPointsCount(roundModel);

        }

        private async Task<PlayGameView>FinalPointsCount(PlayGameView roundModel)
        {
            var dealer = GetDealer(roundModel.Users);

            foreach (var player in roundModel.Users)
            {
                if (player.PlayerRole == PlayerRoleEnumView.Dealer)
                {
                    player.PlayerStatus = dealer.PlayerStatus;
                    continue;
                }
                if (player.PlayerStatus != PlayerStatusEnumView.DefaultValue)
                {
                    continue;
                }
                if (player.CardSum > dealer.CardSum &&
                    player.CardSum < TwentyOnePoint)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Winner;
                }
                if (player.CardSum < TwentyOnePoint &&
                    player.CardSum > dealer.CardSum)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Winner;
                }
                if (player.CardSum < TwentyOnePoint &&
                    player.CardSum < dealer.CardSum)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Lose;
                }
                if (player.CardSum < dealer.CardSum &&
                    dealer.CardSum < TwentyOnePoint)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Lose;
                }
                if (player.CardSum == dealer.CardSum)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Draw;
                }
            }

            roundModel.IsResultComplete = true;
            return roundModel;
        }

        private void UpdateStatus(PlayGameView roundModel)
        {
            foreach (var player in roundModel.Users)
            {
                int status = (int)player.PlayerStatus;
                _playersGameRepository.UpdateStatus(player.GameId, player.PlayerId, status);
            }
        }

        private async Task<int> CreateGame(string name, int botNumber)
        {
            var game = new Game();
            game.Id = await _gameRepository.Add(game);
            return game.Id;
        }

        private async Task<List<Round>> CreateHuman(string name, int gameId)
        {
            Player player;
            player = await _playerRepository.GetHuman(name);

            if (player == null)
            {
                player = new Player { Name = name };
                player.PlayerRole = PlayerRole.Human;
                player.Id = await _playerRepository.Add(player);
            }

            await AddPlayerToCurrentGame(player, gameId);
            return await CreateFirstRound(player, gameId);
        }

        private async Task<List<Round>> CreateBots(int botNumber, int gameId)
        {
            var rounds = new List<Round>();
            IEnumerable<Player> players;

            players = await _playerRepository.GetBots(botNumber);

            foreach (var player in players)
            {
                await AddPlayerToCurrentGame(player, gameId);
                rounds.AddRange(await CreateFirstRound(player, gameId));
            }
            return rounds;
        }

        private async Task<List<Round>> CreateDealer(int gameId)
        {
            var dealer = await _playerRepository.GetDealer();
            dealer.PlayerRole = PlayerRole.Dealer;
            await AddPlayerToCurrentGame(dealer, gameId);
            return await CreateFirstRound(dealer, gameId);
        }

        private async Task<List<Round>> CreateFirstRound(Player player, int gameId)
        {
            var rounds = new List<Round>();

            var firstRound = new Round();
            var secondRound = new Round();

            var firstCard = new Card();
            var secondCard = new Card();

            firstCard = await _cardRepository.GetRandom();
            secondCard = await _cardRepository.GetRandom();

            firstRound.GameId = gameId;
            firstRound.PlayerId = player.Id;
            firstRound.CardId = firstCard.Id;
            firstRound.Id = await _roundRepository.Add(firstRound, gameId);

            secondRound.GameId = gameId;
            secondRound.PlayerId = player.Id;
            secondRound.CardId = secondCard.Id;
            secondRound.Id = await _roundRepository.Add(secondRound, gameId);

            firstRound.Card = firstCard;
            firstRound.Player = player;
            rounds.Add(firstRound);

            secondRound.Card = secondCard;
            secondRound.Player = player;
            rounds.Add(secondRound);

            return rounds;
        }

        private async Task<Round> CreateNextRoundForPlayers(Player player, int gameId)
        {
            var round = new Round();
            var card = new Card();

            card = await _cardRepository.GetRandom();

            round.PlayerId = player.Id;
            round.GameId = gameId;
            round.CardId = card.Id;
            round.Player = player;
            round.Card = card;

            await _roundRepository.Add(round, round.GameId);

            return round;
        }

        private async Task CreateNextRoundForDealer(PlayGameViewItem dealer)
        {
            var round = new Round();
            var card = new Card();

            card = await _cardRepository.GetRandom();

            round.PlayerId = dealer.PlayerId;
            round.GameId = dealer.GameId;
            round.CardId = card.Id;

            var cardViewModel = new CardGameViewItem();
            cardViewModel.Rank = card.Rank.ToString();
            cardViewModel.Suit = card.Suit.ToString();
            cardViewModel.Value = card.Value;

            dealer.CardSum += card.Value;

            dealer.Cards.Add(cardViewModel);
            await _roundRepository.Add(round, round.GameId);
        }

        private void CheckAce(Card card, int playerCardSum)
        {
            if (card.Rank == CardRank.Ace &&
                card.Value + playerCardSum > TwentyOnePoint)
            {                
                    card.Value = 1;
            }
        }

        private async Task CheckDealer(List<PlayGameViewItem> players)
        {
            var dealer = GetDealer(players);

            while (dealer.CardSum < DealerPointBorder)
            {
                await CreateNextRoundForDealer(dealer);
            }
        }

        private PlayGameViewItem GetHumanPlayer(List<PlayGameViewItem> players)
        {
            return players.Where(x => x.PlayerRole == PlayerRoleEnumView.Human).FirstOrDefault();
        }

        private PlayGameViewItem GetDealer(List<PlayGameViewItem> players)
        {
            return players.Where(x => x.PlayerRole == PlayerRoleEnumView.Dealer).FirstOrDefault();
        }

        private async Task<PlayGameView> MappingToViewModel(List<Round> rounds)
        {
            var result = rounds.GroupBy(p => p.Player.Name);
            var roundViewModel = new PlayGameView();
            var playerGames = await _playersGameRepository.GetAll(rounds.First().GameId);

            foreach (var round in result)
            {
                var userModel = new PlayGameViewItem();
                userModel.UserName = round.Key;

                foreach (var item in round)
                {
                    var cardViewModel = new CardGameViewItem();

                    userModel.PlayerId = item.PlayerId;
                    userModel.GameId = item.GameId;
                    userModel.PlayerRole = (PlayerRoleEnumView)item.Player.PlayerRole;
                    userModel.PlayerStatus = (PlayerStatusEnumView)playerGames
                        .Where(g => g.PlayerId == item.PlayerId)
                        .FirstOrDefault().Status;
                    cardViewModel.Rank = item.Card.Rank.ToString();
                    cardViewModel.Suit = item.Card.Suit.ToString();
                    cardViewModel.Value = item.Card.Value;
                    userModel.CardSum += item.Card.Value;

                    userModel.Cards.Add(cardViewModel);
                }
                roundViewModel.Users.Add(userModel);
            }
            roundViewModel.GameId = rounds[0].GameId;

            return roundViewModel;
        }

        private async Task AddPlayerToCurrentGame(Player player, int gameId)
        {
            await _playersGameRepository.AddPlayer(player, gameId);
        }

        private async Task CheckRules(PlayGameView roundModel)
        {
            foreach (var player in roundModel.Users)
            {
                if (player.CardSum == TwentyOnePoint)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Winner;
                }
                if (player.CardSum > TwentyOnePoint)
                {
                    player.PlayerStatus = PlayerStatusEnumView.Lose;
                }
            }
        }
    }
}
