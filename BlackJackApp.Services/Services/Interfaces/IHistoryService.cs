﻿using BlackJackApp.ViewModels.HistoryModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackApp.Services.Interfaces
{
    public interface IHistoryService
    {
        Task<DetailsHistoryView> Details(int gameId);
        Task<GetGamesHistoryView> GetGames(int offset);
    }
}
