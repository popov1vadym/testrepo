﻿using BlackJackApp.ViewModels;
using BlackJackApp.ViewModels.GameModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackApp.Services.Interfaces
{
    public interface IGameService
    {
        Task<GetPlayersGameView> GetPlayers();
        Task<int> Create(CreateGameView viewFromUI);
        Task<PlayGameView> Play(int id, GetCardOption param);
    }
}
