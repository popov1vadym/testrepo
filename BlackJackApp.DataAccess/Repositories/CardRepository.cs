﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using BlackJackApp.DataAccess.Repositories.Interfaces;
using BlackJackApp.Entities.Entities;
using Dapper;

namespace BlackJackApp.DataAccess.Repositories
{
    public class CardRepository : ICardRepository
    {
        private readonly string _connectionString;
        public CardRepository(string con)
        {
            _connectionString = con;
        }

        public async Task<Card> GetRandom()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return await connection.QueryFirstAsync<Card>(@"SELECT TOP 1 * 
                                                                FROM Cards 
                                                                ORDER BY newid()");
            }
        }
    }
}
