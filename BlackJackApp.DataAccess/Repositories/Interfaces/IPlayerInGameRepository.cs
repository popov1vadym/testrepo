﻿using BlackJackApp.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackApp.DataAccess.Repositories.Interfaces
{
    public interface IPlayerInGameRepository
    {
        Task AddPlayer(Player player, int gameId);
        Task<PlayerInGame> GetStatus(int playerId, int gameId);
        Task<PlayerInGame> Get(int gameId);
        Task UpdateStatus(int gameId, int playerId, int playerStatus);
        Task<List<PlayerInGame>> GetAll(int gameId);
    }
}
