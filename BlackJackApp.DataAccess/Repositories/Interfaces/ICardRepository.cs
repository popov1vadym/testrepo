﻿using System.Threading.Tasks;
using BlackJackApp.Entities.Entities;

namespace BlackJackApp.DataAccess.Repositories.Interfaces
{
    public interface ICardRepository
    {
        Task<Card> GetRandom();
    }
}
