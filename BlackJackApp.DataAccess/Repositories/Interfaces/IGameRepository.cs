﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlackJackApp.Entities.Entities;

namespace BlackJackApp.DataAccess.Repositories.Interfaces
{
    public interface IGameRepository
    {
        Task<int> Add(Game game);
        Task<IEnumerable<Game>> GetAll();
        Task<IEnumerable<Game>> GetLastTen(int offset);
        Task<IEnumerable<Player>> Get(int gameId);
    }
}
