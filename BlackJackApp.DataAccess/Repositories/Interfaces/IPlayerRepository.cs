﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlackJackApp.Entities.Entities;

namespace BlackJackApp.DataAccess.Repositories.Interfaces
{
    public interface IPlayerRepository
    {
        Task<int> Add(Player player);
        Task<IEnumerable<string>> GetAll();
        Task<Player> GetHuman(string name);
        Task<IEnumerable<Player>> GetBots(int botNumber);
        Task<Player> GetDealer();
    }
}
