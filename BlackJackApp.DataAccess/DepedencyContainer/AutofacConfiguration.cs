﻿using Autofac;
using BlackJackApp.DataAccess.Repositories;
using BlackJackApp.DataAccess.Repositories.Interfaces;

namespace BlackJackApp.DAL.DepedencyContainer
{
    public class AutofacConfiguration
    {
        public static void ConfigureContainer(ContainerBuilder builder, string connectionString)
        {
            builder.Register(c => new PlayerRepository(connectionString))
                    .As<IPlayerRepository>()
                    .InstancePerRequest();
            builder.Register(c => new GameRepository(connectionString))
                    .As<IGameRepository>()
                    .InstancePerRequest();
            builder.Register(c => new RoundRepository(connectionString))
                    .As<IRoundRepository>()
                    .InstancePerRequest();
            builder.Register(c => new CardRepository(connectionString))
                    .As<ICardRepository>()
                    .InstancePerRequest();
            builder.Register(c => new PlayerInGameRepository(connectionString))
                    .As<IPlayerInGameRepository>()
                    .InstancePerRequest();
        }
    }
}
