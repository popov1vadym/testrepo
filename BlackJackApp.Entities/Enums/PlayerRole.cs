﻿namespace BlackJackApp.Entities.Enums
{
    public enum PlayerRole
    {
        Human = 0,
        Bot = 1,
        Dealer = 2
    }
}
