﻿using BlackJackApp.Entities.Enums;

namespace BlackJackApp.Entities.Entities
{
    public class Player : BaseEntity
    {
        public string Name { get; set; }

        public PlayerRole PlayerRole { get; set; }
    }
}
