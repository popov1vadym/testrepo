﻿using BlackJackApp.Entities.Enums;

namespace BlackJackApp.Entities.Entities
{
    public class Card : BaseEntity
    {        
        public CardRank Rank { get; set; }

        public CardSuit Suit { get; set; }

        public int Value { get; set; }
    }
}
