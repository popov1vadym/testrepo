﻿namespace BlackJackApp.ViewModels.GameModels
{
    public class CreateGameView
    {
        public int GameId { get; set; }

        public string PlayerName { get; set; }

        public int BotQuantity { get; set; }
    }
}
