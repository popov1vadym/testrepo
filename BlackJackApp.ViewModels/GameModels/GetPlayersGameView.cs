﻿using System.Collections.Generic;

namespace BlackJackApp.ViewModels.GameModels
{
    public class GetPlayersGameView
    {
        public List<GetPlayersGameViewItem> Users { get; set; }

        public GetPlayersGameView()
        {
            Users = new List<GetPlayersGameViewItem>();
        }
    }

    public class GetPlayersGameViewItem
    {
        public string Name { get; set; }
    }
}
