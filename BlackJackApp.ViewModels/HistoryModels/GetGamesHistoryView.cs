﻿using System;
using System.Collections.Generic;

namespace BlackJackApp.ViewModels.HistoryModels
{
    public class GetGamesHistoryView
    {
        public List<GetGamesHistoryViewItem> Games { get; set; }

        public GetGamesHistoryView()
        {
            Games = new List<GetGamesHistoryViewItem>();
        }
    }

    public class GetGamesHistoryViewItem
    {
        public int GameId { get; set; }

        public string Name { get; set; }

        public DateTime? GameCreationDate { get; set; }
    }
}