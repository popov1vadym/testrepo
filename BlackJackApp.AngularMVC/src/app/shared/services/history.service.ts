﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DetailHistoryView } from '../views/history/detail-history.view';
import { GetGamesHistoryViewItem, GetGamesHistoryView } from '../views/history/show-games-history.view';
import { environment } from '../../../environments/environment';

@Injectable()
export class HistoryService {

    constructor(private http: HttpClient) { }

    getGames(): Observable<GetGamesHistoryView> {
        return this.http.get<GetGamesHistoryView>(environment.historyUrl + 'get');
    }

    getDetails(id: number): Observable<DetailHistoryView> {
        return this.http.get<DetailHistoryView>(environment.historyUrl + 'details/' + id);
    }
}