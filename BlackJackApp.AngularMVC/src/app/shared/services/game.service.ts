﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { StartGameView } from '../../shared/views/game/start-game.view';
import { GetPlayersGameView, GetPlayersGameViewItem } from '../../shared/views/game/players.view';
import { RoundGameView } from '../views/game/round-game.view';
import { GetCardOption } from '../views/enums/get-card-option-enum';
import { environment } from '../../../environments/environment';

@Injectable()
export class GameService {

    constructor(private http: HttpClient) { }

    getPlayers(): Observable<GetPlayersGameView> {
        return this.http.get<GetPlayersGameView>(environment.gameUrl + 'getPlayers');
    }

    createGame(startModel: StartGameView): Observable<number> {
        return this.http.post<number>(environment.gameUrl + 'create', startModel);
    }

    getRounds(id: number, param: GetCardOption): Observable<RoundGameView> {
        return this.http.get<RoundGameView>(environment.gameUrl + 'play/' + id + '/' + param);
    }
}
