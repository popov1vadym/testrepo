﻿export class GetGamesHistoryView {
    games: GetGamesHistoryViewItem[];
}

export class GetGamesHistoryViewItem {
    gameId: number;
    name: string;
    gameCreationDate: Date | null;
}