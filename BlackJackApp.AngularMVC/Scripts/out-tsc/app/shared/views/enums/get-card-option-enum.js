"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetCardOption;
(function (GetCardOption) {
    GetCardOption[GetCardOption["take"] = 0] = "take";
    GetCardOption[GetCardOption["enough"] = 1] = "enough";
    GetCardOption[GetCardOption["start"] = 2] = "start";
})(GetCardOption = exports.GetCardOption || (exports.GetCardOption = {}));
//# sourceMappingURL=get-card-option-enum.js.map