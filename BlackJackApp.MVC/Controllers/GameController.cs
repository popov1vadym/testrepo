﻿using BlackJackApp.ViewModels.GameModels;
using System.Threading.Tasks;
using System.Web.Mvc;
using BlackJackApp.Services.Interfaces;

namespace BlackJackApp.Controllers.Controllers
{
    public class GameController : Controller
    {
        private IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        public async Task<ActionResult> Start()
        {
            var names = await _gameService.GetPlayers();
            return View(names.Users);
        }

        public async Task<ActionResult> CreateGame(CreateGameView viewModel)
        {
            var gameId = await _gameService.Create(viewModel);
            return RedirectToAction("Play", new { id = gameId, param = 2 });
        }

        public async Task<ActionResult> Play(int id, GetCardOption param)
        {
            ModelState.Clear();
            var result = await _gameService.Play(id, param);
            if ((result.IsResultComplete && param == GetCardOption.Start) || GetCardOption.Enough == param)
            {
                return View("GameFinnished", result);
            }
            if (!result.IsResultComplete && param == GetCardOption.Start)
            {
                return View("CurrentGame", result);
            }
            if (result.IsResultComplete && GetCardOption.Take == param)
            {
                return Json(new
                {
                    id = id,
                    result = result.IsResultComplete,
                    url = Url.Action("Play", "Game", new { id = id, param = 1 })
                });
            }

            if (!result.IsResultComplete && GetCardOption.Take == param)
            {
                return PartialView("CreateNextRoundForPlayers", result);
            }

            return View("Error");
        }
    }
}
