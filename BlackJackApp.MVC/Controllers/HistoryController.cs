﻿using BlackJackApp.Services.Interfaces;
using BlackJackApp.ViewModels.HistoryModels;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BlackJackApp.Controllers.Controllers
{
    public class HistoryController : Controller
    {
        IHistoryService _historyService;

        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }
        
        public async Task<ActionResult> Details(int id)
        {
            var result = await _historyService.Details(id);

            return View(result);
        }

        public async Task<ActionResult> ShowHistoryGrid()
        {
            return View("HistoryGrid");
        }

        public async Task<string> GetGames(int offset = 0)
        {
            var games = new GetGamesHistoryView();
            string json = string.Empty;
            if (offset == 0)
            {
                games = await _historyService.GetGames(offset: 0);
                json = JsonConvert.SerializeObject(games.Games);
                return json;
            }       
            games = await _historyService.GetGames(offset);

            json = JsonConvert.SerializeObject(games.Games);
            return json;
        }
    }
}