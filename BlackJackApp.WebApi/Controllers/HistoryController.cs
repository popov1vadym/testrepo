﻿using BlackJackApp.Services.Interfaces;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlackJackApp.WebApi.Controllers
{
    [RoutePrefix("history")]
    public class HistoryController : ApiController
    {
        IHistoryService _historyService;
        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }

        [HttpGet]
        [Route("get")]
        public async Task<IHttpActionResult> GetGames()
        {
            try
            {
                var games = await _historyService.GetGames(offset: 0);
                return Ok(games);
            }
            catch (Exception e)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("details/{id}")]
        public async Task<IHttpActionResult> Details(int id)
        {
            try
            {
                var result = await _historyService.Details(id);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }           
        }
    }
}
