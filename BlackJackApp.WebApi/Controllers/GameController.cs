﻿using BlackJackApp.Services.Interfaces;
using BlackJackApp.ViewModels.GameModels;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlackJackApp.WebApi.Controllers
{
    [RoutePrefix("game")]
    public class GameController : ApiController
    {
        IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [Route("getPlayers")]
        public async Task<IHttpActionResult> GetPlayers()
        {
            try
            {
                GetPlayersGameView result = await _gameService.GetPlayers();
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }

        [Route("create")]
        public async Task<IHttpActionResult> CreateGame(CreateGameView startModel)
        {
            try
            {
                var result = await _gameService.Create(startModel);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(HttpStatusCode.InternalServerError);

            }
        }

        [Route("play/{id}/{param}")]
        [HttpGet]
        public async Task<IHttpActionResult> Play(int id, GetCardOption param)
        {
            try
            {
                var result = await _gameService.Play(id, param);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }
    }
}
